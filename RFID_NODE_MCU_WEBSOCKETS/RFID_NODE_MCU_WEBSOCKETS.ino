#include "MFRC522.h"
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#define WS_PORT 81
#define RST_PIN 0 // RST-PIN for RC522 - RFID - SPI - Modul GPIO15 
#define SS_PIN  2  // SDA-PIN for RC522 - RFID - SPI - Modul GPIO2
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance

/**
 * Configuração do WI-FI
 */
const char* ssid = "IOT";
const char* password = "xptoxpto";
/**
 * Instanciar Websocket com uma porta especifica
 */
WebSocketsServer webSocket = WebSocketsServer(WS_PORT);
void setup() {
  Serial.begin(9600);    // Initialize serial communications
  SPI.begin();           // Init SPI bus
  mfrc522.PCD_Init();    // Init MFRC522 
   WiFi.mode(WIFI_STA);
  //Iniciar o Modulo Wif-Fi
  WiFi.begin(ssid, password);
  //Aguarda até estar Ligado
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  //DEBUG
  Serial.println("");
  Serial.print("WiFi ligado a: ");
  Serial.println(ssid);

  /*
   * Inicia o Websocket
   */
  webSocket.begin();
  //Regista o Handler para tratar dos eventos do Socket
  webSocket.onEvent(webSocketEvent);
  //DEBUG
  Serial.println("WebSocket Iniciado");

  /* 
   * Imprime o endereço IP do Modulo
   */
  Serial.println(WiFi.localIP());
}
void loop() { 
    webSocket.loop();
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    delay(50);
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    delay(50);
    return;
  }
  // Show some details of the PICC (that is: the tag/card)
  Serial.print(F("Card UID:"));
  dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
  Serial.println();
}

// Helper routine to dump a byte array as hex values to Serial
void dump_byte_array(byte *buffer, byte bufferSize) {
  String code;
  for (byte i = 0; i < bufferSize; i++) {
    //Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    //Serial.print(buffer[i], HEX);
    code+= String(buffer[i],HEX) ;
  }
  webSocket.broadcastTXT("{\"state\":\"" + code + "\"}");
  Serial.println(code);
}

/**
 * Processa os Eventos do Socket
 */
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:
        Serial.printf("[%u] Desligado!\n", num);
        break;
    case WStype_CONNECTED:
        {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Ligado  %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            // Envia a mensagem para o cliente
            webSocket.sendTXT(num, "{\"status\":\"Ligado\"}");
        }
        break;
    case WStype_TEXT:
    //não implementado neste exemplo
        break;
    case WStype_BIN:
    //não implementado neste exemplo
        break;
  }
}

